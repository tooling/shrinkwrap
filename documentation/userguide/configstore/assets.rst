..
 # Copyright (c) 2023, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

###########
assets.yaml
###########

Description
###########

Generation of asset images for testing. This config can be used to build the assets required to run the test script. It builds 1. Buildroot based rootfs that automatically invokes `poweroff -f` 2. Linux kernel image 3. Bootwrapper image

Concrete
########

True

Build-Time Variables
####################

===== =======
btvar default
===== =======
===== =======

Run-Time Variables
##################

===== =======
rtvar default
===== =======
===== =======

