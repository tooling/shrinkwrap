..
 # Copyright (c) 2023, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

############
Config Store
############

Shrinkwrap ships with the following standard set of configs for use
out-of-the-box:

.. toctree::
   :titlesonly:
   :maxdepth: 1

   bootwrapper.rst
   buildroot.rst
   cca-3world.rst
   cca-4world.rst
   cca-edk2.rst
   ffa-hafnium-optee.rst
   ffa-optee.rst
   ffa-tftf.rst
   ns-edk2-optee.rst
   ns-edk2.rst
   ns-preload.rst
