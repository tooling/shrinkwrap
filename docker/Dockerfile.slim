# Copyright (c) 2022, Arm Limited.
# SPDX-License-Identifier: MIT

ARG BASE
FROM ${BASE}

# Ensure apt won't ask for user input.
ENV DEBIAN_FRONTEND=noninteractive

# Setup apt before we install any packages.
RUN apt-get update \
	&& apt-get install auto-apt-proxy --assume-yes \
	&& (timeout --signal=KILL 10s auto-apt-proxy \
		|| apt-get purge --quiet --assume-yes auto-apt-proxy)

# Explicitly install Python and create a venv for pip packages, since Debian
# does not allow us to install pip packages system-wide.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		python3 \
		python3-pip \
		python3-venv
RUN python3 -m venv /pyvenv
ENV PATH="/pyvenv/bin:${PATH}"

# We now install packages required by all the FW components that Shrinkwrap
# supports building. Often there are overlapping components, but we (re)specify
# them for each component in order to keep track of who needs what.

# Install packages that Shrinkwrap relies upon.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		netcat-openbsd \
		openssh-client \
		python3 \
		python3-pip \
		telnet \
		wget
RUN pip3 install \
		termcolor \
		tuxmake \
		pyyaml

# Install packages requried by TF-A.
# From https://trustedfirmware-a.readthedocs.io/en/latest/getting_started/prerequisites.html.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		build-essential \
		git \
		device-tree-compiler

# Install packages requried by TF-A Tests.
# From https://trustedfirmware-a-tests.readthedocs.io/en/latest/getting_started/requirements.html.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		build-essential \
		device-tree-compiler \
		git \
		libxml-libxml-perl \
		perl

# Install packages requried by Hafnium.
# From https://review.trustedfirmware.org/plugins/gitiles/hafnium/hafnium/+/HEAD/docs/GettingStarted.md
# plus extras until the build worked.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		bc \
		bison \
		cpio \
		flex \
		libssl-dev \
		make \
		python3 \
		python3-pip \
		python3-serial
RUN pip3 install \
		fdt

# Install packages requried by OPTEE.
# From https://optee.readthedocs.io/en/latest/building/prerequisites.html
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		adb \
		acpica-tools \
		autoconf \
		automake \
		bc \
		bison \
		build-essential \
		ccache \
		cscope \
		curl \
		device-tree-compiler \
		e2tools \
		expect \
		fastboot \
		flex \
		ftp-upload \
		gdisk \
		libattr1-dev \
		libcap-dev \
		libfdt-dev \
		libftdi-dev \
		libglib2.0-dev \
		libgmp3-dev \
		libhidapi-dev \
		libmpc-dev \
		libncurses5-dev \
		libpixman-1-dev \
		libslirp-dev \
		libssl-dev \
		libtool \
		libusb-1.0-0-dev \
		make \
		mtools \
		netcat-openbsd \
		ninja-build \
		python3-pip \
		python3-serial \
		python-is-python3 \
		rsync \
		swig \
		unzip \
		uuid-dev \
		xdg-utils \
		xterm \
		xz-utils \
		zlib1g-dev
RUN pip3 install \
		cryptography \
		pyelftools


# Install packages requried by EDK2.
# From https://developer.arm.com/documentation/102571/0100/Build-firmware-on-a-Linux-host.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		bison \
		build-essential \
		flex \
		python3 \
		python3-distutils \
		uuid-dev \
		dosfstools \
		mtools \
		parted

# Install packages requried by RMM.
# From https://tf-rmm.readthedocs.io/en/latest/getting_started/getting-started.html.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		build-essential \
		git \
		make \
		ninja-build \
		python3 \
		python3-pip
RUN pip3 install \
		cmake

# Install packages requried by kvmtool.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		build-essential \
		pkg-config

# Install packages requried by Linux.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		build-essential \
		fakeroot \
		kmod \
		rsync

# TODO: Install any packages required by U-Boot, OP-TEE, Trusty, etc.

# Install the aarch64-linux-gnu- toolchain. We use the stock Debian packages for
# this. See below for the aarch64-none-elf- toolchain, which comes from
# developer.arm.com.
RUN apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
		crossbuild-essential-arm64 \
		libc6-dev-arm64-cross

# All non-managed tools will be downloaded here.
RUN mkdir /tools

# Install aarch64 toolchain (aarch64-none-elf-). This is parameterized so the
# caller can easily update the version and architecture for different builds, or
# even omit it by providing the special name "none".
ARG TCH_PKG_NAME_AARCH64
ARG TCH_PATH_AARCH64
COPY assets/${TCH_PKG_NAME_AARCH64} /tools/.
RUN cd /tools \
	&& if [ "${TCH_PKG_NAME_AARCH64}" != "none" ]; then \
	    tar xf ${TCH_PKG_NAME_AARCH64}; \
	fi \
	&& rm ${TCH_PKG_NAME_AARCH64} \
	&& cd -
ENV TCH_PATH_AARCH64="/tools/${TCH_PATH_AARCH64}"
ENV PATH="${TCH_PATH_AARCH64}:${PATH}"

# Set ssh options used by git.
ENV GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

# Set ssh-agent socket path.
ENV SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock

# Create a user.
RUN useradd --create-home shrinkwrap
